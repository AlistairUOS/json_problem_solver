package uk.ac.uos;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

class JSONGUI{ 
	
	//Creates an instance of my code logic which does, getting, parsing and sending.
	JSONProblemSolver problemSolver = new JSONProblemSolver();
	
	/**
	 * The constructor for my Java GUI
	 */
	JSONGUI(){  
		//This creates a frame which is what everything in Java awt is put inside.
		Frame f=new Frame(); 
		
		//I have a text field for entering the URL.
		TextField tf = new TextField("URL");
		//I have an answer box for displaying the result of the run.
		TextArea answerBox = new TextArea("", 100, 100, 1);
		//I have a button to initiate the run.
		Button b=new Button("Run JSON task completor");
		//I am setting the position and size of the button.
		b.setBounds(50,100,500,30);  
		//I have an action listener which will perform the method within is the button is pressed.
		b.addActionListener(new ActionListener(){  
				public void actionPerformed(ActionEvent e){  
					//The URL is found from what has been entered in the URL box.
					String url = tf.getText();
					//This try catch block is necessary to capture
					//any of the exceptions that may be thrown by
					//the following operation.
					try {
						String answer = problemSolver.solve(url);
						answerBox.setText(answer);
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (JSONException e1) {
						e1.printStackTrace();
					}  
				}  
		});
		
		tf.setBounds(50,50,500,20);  
		answerBox.setBounds(50, 150, 500, 300);
		f.add(tf);
		f.add(b);  
		f.add(answerBox);
		f.setSize(600,600);  
		f.setLayout(null);  
		f.setVisible(true);  
	}  

	@SuppressWarnings("unused")
	/**
	 * The main method that is called when this jar is run.
	 * @param args
	 */
	public static void main(String args[]){  
		JSONGUI f = new JSONGUI();  
	}

}  


