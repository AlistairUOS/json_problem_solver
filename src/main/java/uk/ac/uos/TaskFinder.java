package uk.ac.uos;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class TaskFinder {
	
	//This is a class list of all the URLs my tasks are at.
	ArrayList<String> taskURLs = new ArrayList<String>();
	//And this is a list of all my custom Task objects.
	//(This is how I am going to access my answers.)
	ArrayList<JSONTask> listOfTasks = new ArrayList<JSONTask>();
	
	//I need to use my JSONParser from part 1 so I am creating a 
	//version of it here to be used throughout the class.
	JSONParser jsonParser = new JSONParser();
	
	//I am storing these two useful variables for use 
	//throughout the class. My student ID is for validating
	//the pages. The base url is for working out the url 
	//to look for the tasks at because the task urls
	//in the JSON are a semi-relative path.
	public String studentID;
	public String baseUrl;
	public String report = "";
	
	/**
	 * This is the main method that will do the get requests to collect all the tasks.
	 * @param urlString
	 * @throws IOException
	 * @throws JSONException
	 */
	public void runATaskFind(String urlString) throws IOException, JSONException{
		//First I need to find all the URLs for the tasks I am going to 
		//complete.
		createListOfTasks(urlString);
		report += "\n";
		report += "JSON of Tasks:\n";
		//For each of the task URLs I am going to do my parsing.
		for (int i = 0; i < taskURLs.size(); i++) {
			//I am working out the full url for the task here.
			String urlForTask = baseUrl + taskURLs.get(i); 
			//And here I am sending off to fo a GET request for
			//that task based on the URL.
			String currentTaskJSON = getIndividualTaskJSON(urlForTask);
			JSONTask currentJSONTask = null;
			//Just a flag for checking if the parsing of the test failed.
			boolean failedParsing = false;
			//This try catch block is for catching and errors in the tasks.
			//It will then send the error back to the server.
			try {
				currentJSONTask = parseAndGenerateTask(currentTaskJSON);
				currentJSONTask.getAnswer();
			} catch(JSONException e) {
				String errorMessage = e.getMessage();
				int returnCode = returnJSONError(urlForTask, errorMessage);
				report += "Error for " + urlForTask + "\n";
				report += "Error Message = " + errorMessage + "\n";
				report += "Return Code = " + returnCode + "\n";
				//Setting the failing parsing to true. Now I know it failed.
				failedParsing = true;
			}
			//Once I have processed the task I can add it to the list.
			//It will only add it to the list if the parsing was
			//successful.
			if ( failedParsing == false) {
				listOfTasks.add(currentJSONTask);
			}
		}
	}
	
	/**
	 * Need a method in case I have to send back an error.
	 * @param urlString
	 * @param errorMessage
	 * @return int
	 * @throws IOException
	 * @throws JSONException
	 */
	public int returnJSONError(String urlString, String errorMessage) throws IOException, JSONException {
		URL url = new URL(urlString);
		//Making the connection to the url.
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		//Setting the method to POST because I am sending
		//a response.
		connection.setRequestMethod("POST");
		connection.setRequestProperty("User-Agent", "Mozilla/5.0");
		//Setting the language type for the response.
		connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		//This is confirming that I am going to be 
		//sending on this output.
		connection.setDoOutput(true);
		
		//These four lines are just sending my message as
		//an output stream.
		DataOutputStream outputter = new DataOutputStream(connection.getOutputStream());
		outputter.writeBytes(errorMessage);
		outputter.flush();
		outputter.close();
		
		int responseCode = connection.getResponseCode();
		//Checking to see that the response code is correct.
		//200 means a good response.
		if ( responseCode != 200 ) {
			throw new JSONException("Boom! Error. Failed to connect to " + url);
		}
		//Returning the response code allows me to test the success of this method in tests.
		return responseCode;
	}
	
	/**
	 * This method is doing the get request to find the JSON which defines the task.
	 * @param urlString
	 * @return String
	 * @throws IOException
	 * @throws JSONException
	 */
	public String getTaskJSON(String urlString) throws IOException, JSONException{
		//In these three lines I am working out the base url
		//and the student ID for validation purposes.
		String[] splitUrl = urlString.split("id=");
		baseUrl = splitUrl[0].replace("/student?", "");
		studentID = splitUrl[1];
		
		
		URL url = new URL(urlString);
		//Making a connection to the url.
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		//Setting the method to GET because I will be receiving.
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent", "Mozilla/5.0");
		int responseCode = connection.getResponseCode();
		//Checking to see that the response code is correct.
		//200 means a good response.
		if ( responseCode != 200 ) {
			throw new JSONException("Boom! Error. Failed to connect to " + url);
		}
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line = "";
		String contents = "";
		//Looping through the reader to build the response from the url as a string.
		while ((line = bufferedReader.readLine()) != null) {
			contents += line;
		}
		bufferedReader.close();
		
		report += "JSON from main url:\n";
		report += contents;
		report += "\n";
		return contents;
	}
	
	/**
	 * This is a GET request method for retrieving the JSON of the task pages.
	 * @param urlString
	 * @return String
	 * @throws IOException
	 * @throws JSONException
	 */
	public String getIndividualTaskJSON(String urlString) throws IOException, JSONException {
		URL url = new URL(urlString);
		//Making a connection to the url.
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		//Setting the method to GET because I will be receiving.
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent", "Mozilla/5.0");
		int responseCode = connection.getResponseCode();
		//Checking to see that the response code is correct.
		//200 means a good response.
		if ( responseCode != 200 ) {
			throw new JSONException("Boom! Error. Failed to connect to " + url);
		}
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line = "";
		String contents = "";
		//Looping through the reader to build the response from the url as a string.
		while ((line = bufferedReader.readLine()) != null) {
			contents += line;
		}
		bufferedReader.close();
		
		report += contents;
		report += "\n";
		return contents;
	}
	
	/**
	 * Storing the list of tasks I must GET in an ArrayList
	 * @param urlString
	 * @throws IOException
	 * @throws JSONException
	 */
	public void createListOfTasks(String urlString) throws IOException, JSONException{
		String taskJSON = getTaskJSON(urlString);
		//Below is the logic for turning a list of objects
		//into a list of strings.
		ArrayList<Object> tasksObjects = parseTaskList(taskJSON);
		ArrayList<String> tasksString = new ArrayList<String>();
		for (int i = 0; i < tasksObjects.size(); i++) {
			String currentTask = (String) tasksObjects.get(i);
			tasksString.add(currentTask);
		}
		this.taskURLs = tasksString;
	}

	/**
	 * Reading the JSON to find the list of tasks.
	 * @param listJSON
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	public ArrayList<Object> parseTaskList(String listJSON) throws IOException, JSONException{
		//I am calling my parser to parse this bit of the JSON.
		jsonParser.parse(listJSON);
		
		//Now I retrieve the tasks from the JSON
		//using my interface via the 'mainObject'.
		JSONObject jsonObject = jsonParser.mainObject;
		String id = (String) jsonObject.objectContents.get("id");
		//I am validating that this is the correct task list
		//based on my student id.
		boolean validTasks = validateTaskSheet(id);
		if (validTasks == false) {
			//Throwing an exception here.
			throw new JSONException("Boom! Error. These tasks are not correct for this URL but for " + id);
		}
		JSONArray jsonArray = (JSONArray) jsonObject.objectContents.get("tasks");
		return (ArrayList<Object>) jsonArray.arrayContents;
	}
	
	
	/**
	 * This method ensures the tasks given are correct for the url.
	 * The ID in the JSON should matched the ID in the url.
	 * @param id
	 * @return
	 */
	public boolean validateTaskSheet(String id) {
		boolean result = false;
		if ( id.equals(studentID)) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}
	
	/**
	 * Using my parser I find the contents of the task and create a task object.
	 * @param taskJSON
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	public JSONTask parseAndGenerateTask(String taskJSON) throws IOException, JSONException {
		//Using my parser from part 1 to parse the JSON 
		//of a task.
		jsonParser.parse(taskJSON);
		
		//Via the 'mainObject' I am accessing he various parts of a task.
		JSONObject jsonObject = jsonParser.mainObject;
		String instruction = (String) jsonObject.objectContents.get("instruction");
		JSONArray jsonArray = (JSONArray) jsonObject.objectContents.get("parameters");
		Object parameter1 = jsonArray.arrayContents.get(0);
		//I have to turn the objects into strings because my JSONTask
		//assumes all parameters are strings.
		String parameter1String = normaliseObjectToString(parameter1);
		Object parameter2 = jsonArray.arrayContents.get(1);
		String parameter2String = normaliseObjectToString(parameter2);
		String responseURL = (String) jsonObject.objectContents.get("response URL");
		
		//Once I have extracted the relevant information from the JSON
		//I can create a JSONTask which is based off of these.
		return new JSONTask(instruction, parameter1String, parameter2String, responseURL);
	}
	
	/**
	 * My JSONTask is only going to take string parameters. Here I turn longs from the parser into strings.
	 * @param parameter
	 * @return
	 * @throws JSONException
	 */
	public String normaliseObjectToString(Object parameter) throws JSONException {
		String result = "";
		//By using instance of I am able to tell if
		//an object is a string or a long. If it is
		//long I can cast it into a string so that 
		//both parameters are strings.
		if (parameter instanceof String) {
			result = (String) parameter;
		} else if (parameter instanceof Long) {
			result = parameter.toString();
		} else {
			//If it is neither a string or a long
			//then there has been an error because both the
			//parameters in all the tasks should be one of these.
			throw new JSONException("Boom! Error. Invalid parameter " + parameter);
		}
		return result;
	}

}
