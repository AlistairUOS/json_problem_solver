package uk.ac.uos;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import uk.ac.uos.JSONException;

public class AnswerSender {
	
	//A list of all the URLs I have to send my answers to.
	ArrayList<String> answerURLs = new ArrayList<String>();
	//A list of all the answers that I am going to send.
	ArrayList<String> answers = new ArrayList<String>();
	
	//A variable to store the base URL for the answers.
	public String baseUrl;
	
	/**
	 * 	This is the main method that will be called.
	 *  This method will take a list of tasks and send out the
	 *  answers the answer URLs based on the base URL.
	 *  Returning a string in case error messages replies
	 *  must be checked.
	 * @param baseUrl
	 * @param listOfTasks
	 * @return String
	 * @throws JSONException
	 * @throws IOException
	 */
	public String runAnAnswerSend(String baseUrl, ArrayList<JSONTask> listOfTasks) throws JSONException, IOException {
		//In order to use the answer URLs I need to base them
		//on this initial URL from the tasks.
		setBaseUrl(baseUrl);
		
		//Turns a list of tasks into answers and URLs for ease of use.
		parseListOfTasks(listOfTasks);
		
		//A list to store any error messages in sending the answers.
		String message = "";
		
		//Now I am iterating through the number of tasks and sending off the answers.
		for (int i = 0; i < listOfTasks.size(); i++) {
			String currentURL = answerURLs.get(i);
			message += "URL = " + baseUrl + currentURL + "\n";
			String currentAnswer = answers.get(i);
			message += "Instruction = " + listOfTasks.get(i).instruction + "\n";
			message += "First parameter = " + listOfTasks.get(i).parameter1 + "\n";
			message += "Second parameter = " + listOfTasks.get(i).parameter2 + "\n";
			message += "Answer = " + currentAnswer + "\n";
			int returnCode = 0;
			try {
				returnCode = sendAnswerToURL(currentAnswer, currentURL);
			} catch(JSONException e) {
				message += e.getMessage() + "\n";
			}
			message += "Return code = " + returnCode + "\n";
			message += "\n";
		}
		return message;
	}
	

	/**
	 * 	This method takes a list of tasks and converts them to a 
	 *  list of answers and URLs.
	 * @param listOfTasks
	 * @throws JSONException
	 */
	public void parseListOfTasks(ArrayList<JSONTask> listOfTasks) throws JSONException {
		for (int i = 0; i < listOfTasks.size(); i++) {
			JSONTask currentTask = listOfTasks.get(i);
			String currentURL = currentTask.responseURL;
			String currentAnswer = currentTask.getAnswer();
			answerURLs.add(currentURL);
			answers.add(currentAnswer);
		}
	}
	
	/**
	 * Setting the base URL from outside this class
	 * @param input
	 */
	public void setBaseUrl(String input) {
		this.baseUrl = input;
	}
	
	/**
	 * 	This is a POSTing method to send one answer to its URL.
	 *  It is also validating if the answer is correct based on the response.
	 * @param answer
	 * @param urlString
	 * @return int
	 * @throws IOException
	 * @throws JSONException
	 */
	public int sendAnswerToURL(String answer, String urlString) throws IOException, JSONException {
		URL url = new URL(baseUrl + urlString);
		//Making the connection to the URL.
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		//Setting the method to POST because I am sending
		//a response.
		connection.setRequestMethod("POST");
		connection.setRequestProperty("User-Agent", "Mozilla/5.0");
		//Setting the language type for the response.
		connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		//This is confirming that I am going to be 
		//sending on this output.
		connection.setDoOutput(true);
		
		//These four lines are just sending my message as
		//an output stream.
		DataOutputStream outputter = new DataOutputStream(connection.getOutputStream());
		outputter.writeBytes(answer);
		outputter.flush();
		outputter.close();
		
		int responseCode = connection.getResponseCode();
		//Checking to see that the response code is correct.
		//200 means a good response.
		if ( responseCode != 200 ) {
			//If the code is not 200 then the answer is assumed to be wrong.
			throw new JSONException("Boom! Error. Apparantly incorrect answer " + answer + " sent to " + urlString + " (possibly due to an unsupported operation such as subtract on the server).");
		}
		//Returning the response code allows me to test the success of this method in tests.
		return responseCode;
	}

}
