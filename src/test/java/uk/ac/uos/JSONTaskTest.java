package uk.ac.uos;

import static org.junit.Assert.*;

import org.junit.Test;

public class JSONTaskTest {

	JSONTask jt;

	@Test
	public void adding1And1Gives2() {
		jt = new JSONTask("add", "1", "1", "/answer/d3ae45");
		int result = jt.add("1", "1");
		assertEquals(2, result);
	}
	
	@Test
	public void multiplying6And2Gives12() {
		jt = new JSONTask("multiply", "6", "2", "/answer/d3ae45");
		int result = jt.multiply("6", "2");
		assertEquals(12, result);
	}
	
	@Test
	public void concatenatingHelloAndWorldGivesHelloWorld() {
		jt = new JSONTask("concatenate", "Hello", "World", "/answer/d3ae45");
		String result = jt.concatenate("Hello", "World");
		assertEquals("HelloWorld", result);
	}
	
	@Test
	public void gettingAnswerForAddCase() throws JSONException {
		jt = new JSONTask("add", "176", "2", "/answer/d3ae45");
		String result = jt.getAnswer();
		assertEquals("178", result);
	}
	
	@Test
	public void gettingAnswerForMultiplyCase() throws JSONException {
		jt = new JSONTask("multiply", "10", "2", "/answer/d3ae45");
		String result = jt.getAnswer();
		assertEquals("20", result);
	}
	
	@Test
	public void gettingAnswerForConcatenateCase() throws JSONException {
		jt = new JSONTask("concat", "Uni", "Time", "/answer/d3ae45");
		String result = jt.getAnswer();
		assertEquals("UniTime", result);
	}
	
	@Test
	public void invalidInstructionThrowsJSONException() {
		boolean thrown = false;
		String message = "";
		jt = new JSONTask("divide", "4", "3", "/answer/d3ae45");
		
		try {
			jt.getAnswer();
		} catch (JSONException e) {
			thrown = true;
			message = e.getMessage();
		}
		
		assertEquals(true, thrown);
		assertEquals("Boom! Error. Unexpected instruction divide", message);
	}


}
