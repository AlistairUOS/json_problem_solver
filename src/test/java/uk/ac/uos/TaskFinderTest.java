package uk.ac.uos;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class TaskFinderTest {

	TaskFinder tf;

	@Before
	public void setUp() throws Exception {
		tf = new TaskFinder();
	}

	@Test
	public void normalisingAStringToAString() throws JSONException {
		String result = tf.normaliseObjectToString("Words");
		assertEquals("Words", result);
	}
	
	@Test
	public void normalisingALongToAString() throws JSONException {
		String result = tf.normaliseObjectToString((long) 67);
		assertEquals("67", result);
	}
	
	@Test
	public void tryingToNormaliseADoubleIsInvalid() throws JSONException {
		boolean thrown = false;
		String message = "";
		try {
			tf.normaliseObjectToString((double) 6.7);
		} catch(Exception e) {
			thrown = true;
			message = e.getMessage();
		}
		assertEquals(true, thrown);
		assertEquals("Boom! Error. Invalid parameter 6.7", message);
	}
	
	@Test
	public void generateTaskFromJSON() throws IOException, JSONException {
		JSONTask jsonTask = tf.parseAndGenerateTask("{\"instruction\":\"add\",\"parameters\":[8054,2119],\"response URL\":\"/answer/8473\"}\r\n");
		String instruction = jsonTask.instruction;
		String parameter1 = jsonTask.parameter1;
		String parameter2 = jsonTask.parameter2;
		String responseURL = jsonTask.responseURL;
		
		assertEquals("add", instruction);
		assertEquals("8054", parameter1);
		assertEquals("2119", parameter2);
		assertEquals("/answer/8473", responseURL);
	}
	
	@Test
	public void myIDValidatesCorrectly() {
		tf.studentID = "s188216";
		boolean result = tf.validateTaskSheet("s188216");
		assertEquals(true, result);
	}
	
	@Test
	public void fakeIDDoesNotValidate() {
		tf.studentID = "s188216";
		boolean result = tf.validateTaskSheet("s111111");
		assertEquals(false, result);
	}
	
	@Test
	public void accuratelyParsingATaskJSONPage() throws IOException, JSONException {
		tf.studentID = "s188216";
		ArrayList<Object> listOfTasks = tf.parseTaskList("{\"id\":\"s188216\",\"tasks\":[\"/task/8473\",\"/task/7225\",\"/task/983\",\"/task/4963\",\"/task/8413\",\"/task/2915\",\"/task/9946\",\"/task/1621\",\"/task/6432\",\"/task/1090\"]}\r\n");
		String task1 = (String) listOfTasks.get(0);
		String task2 = (String) listOfTasks.get(1);
		String task3 = (String) listOfTasks.get(2);
		String task4 = (String) listOfTasks.get(3);
		String task5 = (String) listOfTasks.get(4);
		String task6 = (String) listOfTasks.get(5);
		String task7 = (String) listOfTasks.get(6);
		String task8 = (String) listOfTasks.get(7);
		String task9 = (String) listOfTasks.get(8);
		String task10 = (String) listOfTasks.get(9);
		
		assertEquals("/task/8473", task1);
		assertEquals("/task/7225", task2);
		assertEquals("/task/983", task3);
		assertEquals("/task/4963", task4);
		assertEquals("/task/8413", task5);
		assertEquals("/task/2915", task6);
		assertEquals("/task/9946", task7);
		assertEquals("/task/1621", task8);
		assertEquals("/task/6432", task9);
		assertEquals("/task/1090", task10);
	}
	
	@Test
	public void parsingTaskJSONWithIncorrectID() throws IOException {
		boolean thrown = false;
		String message = "";
		
		try {
			tf.parseTaskList("{\"id\":\"s9\",\"tasks\":[\"/task/8473\",\"/task/7225\",\"/task/983\",\"/task/4963\",\"/task/8413\",\"/task/2915\",\"/task/9946\",\"/task/1621\",\"/task/6432\",\"/task/1090\"]}\r\n");
		} catch(JSONException e) {
			thrown = true;
			message = e.getMessage();
		}
		
		assertEquals(true, thrown);
		assertEquals("Boom! Error. These tasks are not correct for this URL but for s9", message);
	}
	
	@Test
	public void gettingMyTaskJSONCorrectly() throws Exception {
		String taskJSON = tf.getTaskJSON("http://i2j.openode.io/student?id=s188216");
		String expected = "{\"id\":\"s188216\",\"tasks\":[\"/task/8473\",\"/task/7225\",\"/task/983\",\"/task/4963\",\"/task/8413\",\"/task/2915\",\"/task/9946\",\"/task/1621\",\"/task/6432\",\"/task/1090\"]}";
		assertEquals(expected, taskJSON);
	}
	
	@Test
	public void connectingToWrongURL() {
		boolean thrown = false;
		String message = "";
		try {
			tf.getTaskJSON("http://i2j.openode.io/student?id=s123456");
		} catch(Exception e) {
			thrown = true;
			message = e.getMessage();
		}
		assertEquals(true, thrown);
		assertEquals("Boom! Error. Failed to connect to http://i2j.openode.io/student?id=s123456", message);
	}
	
	@Test
	public void generatingTheListOfTasks() throws Exception {
		tf.createListOfTasks("http://i2j.openode.io/student?id=s188216");
		ArrayList<String> listOfTasks = tf.taskURLs;
		String task1 = listOfTasks.get(0);
		String task2 = listOfTasks.get(1);
		String task3 = listOfTasks.get(2);
		String task4 = listOfTasks.get(3);
		String task5 = listOfTasks.get(4);
		String task6 = listOfTasks.get(5);
		String task7 = listOfTasks.get(6);
		String task8 = listOfTasks.get(7);
		String task9 = listOfTasks.get(8);
		String task10 = listOfTasks.get(9);
		
		assertEquals("/task/8473", task1);
		assertEquals("/task/7225", task2);
		assertEquals("/task/983", task3);
		assertEquals("/task/4963", task4);
		assertEquals("/task/8413", task5);
		assertEquals("/task/2915", task6);
		assertEquals("/task/9946", task7);
		assertEquals("/task/1621", task8);
		assertEquals("/task/6432", task9);
		assertEquals("/task/1090", task10);
	}
	
	@Test
	public void gettingTheJSONForATask() throws IOException, JSONException {
		String taskJSON = tf.getIndividualTaskJSON("http://i2j.openode.io/task/8473");
		assertEquals("{\"instruction\":\"add\",\"parameters\":[8054,2119],\"response URL\":\"/answer/8473\"}", taskJSON);
	}
	
	@Test
	public void gettingTheJSONForATaskWithIncorrectURL() throws IOException, JSONException {
		boolean thrown = false;
		String message = "";
		try {
			tf.getIndividualTaskJSON("http://i2j.openode.io/task/847399999999");
		} catch(JSONException e) {
			thrown = true;
			message = e.getMessage();
		}
		assertEquals(true, thrown);
		assertEquals("Boom! Error. Failed to connect to http://i2j.openode.io/task/847399999999", message);
	}
	
	@Test
	public void returningErrorMessageToCorrectURL() throws IOException, JSONException {
		int result = tf.returnJSONError("http://i2j.openode.io/task/6432", "Boom! Error. Invalid JSON.");
		assertEquals(200, result);
	}
	
	@Test
	public void returningErrorMessageToIncorrectURLThrowsException() throws IOException, JSONException {
		boolean thrown = false;
		String message = "";
		try {
			tf.returnJSONError("http://i2j.openode.io/task/84731111", "Boom! Error. Invalid JSON.");
		} catch(JSONException e) {
			thrown = true;
			message = e.getMessage();
		}
		assertEquals(true, thrown);
		assertEquals("Boom! Error. Failed to connect to http://i2j.openode.io/task/84731111", message);
	}
	
	@Test
	public void buildingASetOfTasksFromURL() throws Exception {
		tf.runATaskFind("http://i2j.openode.io/student?id=s188216");
	}

}
