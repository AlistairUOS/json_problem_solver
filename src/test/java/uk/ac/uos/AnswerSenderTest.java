package uk.ac.uos;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class AnswerSenderTest {
	
	AnswerSender as;

	@Before
	public void setUp() throws Exception {
		as = new AnswerSender();
	}
	
	@Test
	public void canCorrectlyParseAListOfTasks() throws JSONException {
		JSONTask task1 = new JSONTask("add", "5", "4", "/answer/d3ae45");
		JSONTask task2 = new JSONTask("multiply", "2", "10", "/answer/rt45u");		
		ArrayList<JSONTask> listOfTasks = new ArrayList<JSONTask>();
		listOfTasks.add(task1);
		listOfTasks.add(task2);

		as.parseListOfTasks(listOfTasks);
		ArrayList<String> listOfURLs = as.answerURLs;
		assertEquals("/answer/d3ae45", listOfURLs.get(0));
		assertEquals("/answer/rt45u", listOfURLs.get(1));
		ArrayList<String> listOfAnswers = as.answers;
		assertEquals("9", listOfAnswers.get(0));
		assertEquals("20", listOfAnswers.get(1));
	}
	
	@Test
	public void testingICanSetABaseURL() {
		as.setBaseUrl("www.bt.com");
		assertEquals("www.bt.com", as.baseUrl);
	}
	
	@Test
	public void canCorrectlySendAnAnswerToAURL() throws IOException, JSONException {
		as.setBaseUrl("http://i2j.openode.io");
		int result = as.sendAnswerToURL("10173", "/answer/8473");
		assertEquals(200, result);
	}
	
	@Test
	public void incorrectAnswerThrowsException() throws IOException, JSONException {
		as.setBaseUrl("http://i2j.openode.io");
		boolean thrown = false;
		String message = "";
		
		try {
			as.sendAnswerToURL("67", "/answer/8473");
		} catch(JSONException e) {
			thrown = true;
			message= e.getMessage();
		}
		assertEquals(true, thrown);
		assertEquals("Boom! Error. Apparantly incorrect answer 67 sent to /answer/8473 (possibly due to an unsupported operation such as subtract on the server).", message);
	}
	
	@Test
	public void testingAnOverallAnswerSendProcess() throws JSONException, IOException {
		JSONTask task1 = new JSONTask("add", "8054", "2119", "/answer/8473");
		ArrayList<JSONTask> listOfTasks = new ArrayList<JSONTask>();
		listOfTasks.add(task1);
		as.runAnAnswerSend("http://i2j.openode.io", listOfTasks);
	}

}
