package uk.ac.uos;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JSONProblemSolverTest {

	JSONProblemSolver jps;

	@Before
	public void setUp() throws Exception {
		jps = new JSONProblemSolver();
	}

	@Test
	public void testingWritingToAFile() throws IOException {
		jps.writeReportToFile("Test line");
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String stringDate = dateFormat.format(date);
		String filename = "JSON_Problem_Solver_Report_" + stringDate +".txt";
		File file = new File(filename);
		FileReader fileReader = new FileReader(filename);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String result = "";
		String currentLine;
		while ((currentLine = bufferedReader.readLine()) != null) {
			result += currentLine;
		}
		assertEquals("Test line", result);
		file.delete();
		bufferedReader.close();
	}
	
	@Test
	public void testingTheWholeProcess() throws IOException, JSONException {
		String answer = jps.solve("http://i2j.openode.io/student?id=s188216");
		String expected = "JSON from main url:\n" + 
				"{\"id\":\"s188216\",\"tasks\":[\"/task/8473\",\"/task/7225\",\"/task/983\",\"/task/4963\",\"/task/8413\",\"/task/2915\",\"/task/9946\",\"/task/1621\",\"/task/6432\",\"/task/1090\"]}\n" + 
				"\n" + 
				"JSON of Tasks:\n" + 
				"{\"instruction\":\"add\",\"parameters\":[8054,2119],\"response URL\":\"/answer/8473\"}\n" + 
				"{\"instruction\":\"multiply\",\"parameters\":[7985,1357],\"response URL\":\"/answer/7225\"}\n" + 
				"{\"instruction\":\"concat\",\"parameters\":[\"7803\",\"6137\"],\"response URL\":\"/answer/983\"}\n" + 
				"{\"instruction\":\"add\",\"parameters\":[6884,\"8905\"],\"response URL\":\"/answer/4963\"}\n" + 
				"{\"instruction\":\"add\",\"parameters\":[\"6251\",5945],\"response URL\":\"/answer/8413\"}\n" + 
				"{\"instruction\":\"multiply\",\"parameters\":[\"4415\",\"7506\"],\"response URL\":\"/answer/2915\"}\n" + 
				"{\"instruction\":\"concat\",\"parameters\":[4786,\"3522\"],\"response URL\":\"/answer/9946\"}\n" + 
				"{\"instruction\":\"concat\",\"parameters\":[\"8208\",2089],\"response URL\":\"/answer/1621\"}\n" + 
				"{\"instruction\":\"subtract\",\"parameters\":[3315,2276],\"response URL\":\"/answer/6432\"}\n" + 
				"Error for http://i2j.openode.io/task/6432\n" + 
				"Error Message = Boom! Error. Unexpected instruction subtract\n" + 
				"Return Code = 200\n" + 
				"\"add up the two numbers 2647 and 8258\"\n" + 
				"Error for http://i2j.openode.io/task/1090\n" + 
				"Error Message = BOOM! Error. This is not an object.\n" + 
				"Return Code = 200\n" + 
				"\n" + 
				"\n" + 
				"URL = http://i2j.openode.io/answer/8473\n" + 
				"Instruction = add\n" + 
				"First parameter = 8054\n" + 
				"Second parameter = 2119\n" + 
				"Answer = 10173\n" + 
				"Return code = 200\n" + 
				"\n" + 
				"URL = http://i2j.openode.io/answer/7225\n" + 
				"Instruction = multiply\n" + 
				"First parameter = 7985\n" + 
				"Second parameter = 1357\n" + 
				"Answer = 10835645\n" + 
				"Return code = 200\n" + 
				"\n" + 
				"URL = http://i2j.openode.io/answer/983\n" + 
				"Instruction = concat\n" + 
				"First parameter = 7803\n" + 
				"Second parameter = 6137\n" + 
				"Answer = 78036137\n" + 
				"Return code = 200\n" + 
				"\n" + 
				"URL = http://i2j.openode.io/answer/4963\n" + 
				"Instruction = add\n" + 
				"First parameter = 6884\n" + 
				"Second parameter = 8905\n" + 
				"Answer = 15789\n" + 
				"Return code = 200\n" + 
				"\n" + 
				"URL = http://i2j.openode.io/answer/8413\n" + 
				"Instruction = add\n" + 
				"First parameter = 6251\n" + 
				"Second parameter = 5945\n" + 
				"Answer = 12196\n" + 
				"Return code = 200\n" + 
				"\n" + 
				"URL = http://i2j.openode.io/answer/2915\n" + 
				"Instruction = multiply\n" + 
				"First parameter = 4415\n" + 
				"Second parameter = 7506\n" + 
				"Answer = 33138990\n" + 
				"Return code = 200\n" + 
				"\n" + 
				"URL = http://i2j.openode.io/answer/9946\n" + 
				"Instruction = concat\n" + 
				"First parameter = 4786\n" + 
				"Second parameter = 3522\n" + 
				"Answer = 47863522\n" + 
				"Return code = 200\n" + 
				"\n" + 
				"URL = http://i2j.openode.io/answer/1621\n" + 
				"Instruction = concat\n" + 
				"First parameter = 8208\n" + 
				"Second parameter = 2089\n" + 
				"Answer = 82082089\n" + 
				"Return code = 200\n" + 
				"\n";
		assertEquals(expected, answer);
	}

}
